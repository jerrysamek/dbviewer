package eu.tsamek.dbviewer.utils;

import org.junit.Assert;
import org.junit.Test;

public class CryptoUtilsTest {

  @Test
  public void encrypt() throws Exception {

    final var original = "my long text to be encrypted";
    final var encrypted = CryptoUtils.encrypt(original);

    Assert.assertNotEquals(original, encrypted);

    final var decrypted = CryptoUtils.decrypt(encrypted);

    Assert.assertNotEquals(encrypted, decrypted);
    Assert.assertEquals(original, decrypted);
  }
}