package eu.tsamek.dbviewer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.mapping.SimpleUserTypeResolver;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories(basePackages = "eu.tsamek.dbviewer.repository")
public class CassandraConfiguration extends AbstractCassandraConfiguration {

  @Override
  protected String getKeyspaceName() {
    return "dbviewer";
  }

  @Bean
  public CassandraClusterFactoryBean cluster() {
    final var cluster = new CassandraClusterFactoryBean();
    cluster.setContactPoints("127.0.0.1");
    cluster.setPort(9042);
    return cluster;
  }

  @Bean
  public CassandraMappingContext cassandraMapping() {
    final var contextMapping = new BasicCassandraMappingContext();
    contextMapping.setUserTypeResolver(new SimpleUserTypeResolver(cluster().getObject(), getKeyspaceName()));
    return contextMapping;
  }
}
