package eu.tsamek.dbviewer.entities;

import com.google.common.collect.Lists;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Table(value = "connectors")
public class DBConnector {

  @PrimaryKeyColumn(name = "id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
  private UUID id;

  @Column
  private String type;

  @Column
  private String version;

  @Column
  private String className;

  @Column
  private List<String> objects = Lists.newArrayList();

  @Column
  private List<DBConnectorParamType> properties = Collections.emptyList();

  public UUID getId() {
    return id;
  }

  public DBConnector setId(UUID id) {
    this.id = id;
    return this;
  }

  public String getType() {
    return type;
  }

  public DBConnector setType(String type) {
    this.type = type;
    return this;
  }

  public String getVersion() {
    return version;
  }

  public DBConnector setVersion(String version) {
    this.version = version;
    return this;
  }

  public String getClassName() {
    return className;
  }

  public DBConnector setClassName(String className) {
    this.className = className;
    return this;
  }

  public List<String> getObjects() {
    return objects;
  }

  public DBConnector setObjects(List<String> objects) {
    this.objects = objects;
    return this;
  }

  public List<DBConnectorParamType> getProperties() {
    return properties;
  }

  public DBConnector setProperties(List<DBConnectorParamType> properties) {
    this.properties = properties;
    return this;
  }
}
