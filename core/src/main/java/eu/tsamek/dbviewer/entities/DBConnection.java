package eu.tsamek.dbviewer.entities;

import com.datastax.driver.core.DataType;
import com.google.common.collect.Maps;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import java.util.Map;
import java.util.UUID;

@Table(value = "connections")
public class DBConnection {

  @PrimaryKeyColumn(name = "id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
  private UUID id;

  @Column
  private String name;

  @Column
  private String type;

  @Column
  @CassandraType(type = DataType.Name.MAP, typeArguments = {
          DataType.Name.TEXT, DataType.Name.TEXT
  })
  private Map<String, String> properties = Maps.newHashMap();

  public UUID getId() {
    return id;
  }

  public DBConnection setId(UUID id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public DBConnection setName(String name) {
    this.name = name;
    return this;
  }

  public String getType() {
    return type;
  }

  public DBConnection setType(String type) {
    this.type = type;
    return this;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public DBConnection setProperties(Map<String, String> properties) {
    this.properties = properties;
    return this;
  }
}
