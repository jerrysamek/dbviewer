package eu.tsamek.dbviewer.entities;

import com.datastax.driver.core.DataType;
import org.springframework.data.cassandra.mapping.CassandraType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.UserDefinedType;

@UserDefinedType("params_definition")
public class DBConnectorParamType {

  @Column
  @CassandraType(type = DataType.Name.TEXT)
  private String name;

  @Column
  @CassandraType(type = DataType.Name.TEXT)
  private String valueType;

  @Column
  @CassandraType(type = DataType.Name.TEXT)
  private String description;

  @Column
  @CassandraType(type = DataType.Name.BOOLEAN)
  private boolean required = true;

  @Column
  @CassandraType(type = DataType.Name.BOOLEAN)
  private boolean secret = false;

  @Column
  @CassandraType(type = DataType.Name.TEXT)
  private String defaultValue;

  public String getName() {
    return name;
  }

  public DBConnectorParamType setName(String name) {
    this.name = name;
    return this;
  }

  public String getValueType() {
    return valueType;
  }

  public DBConnectorParamType setValueType(String valueType) {
    this.valueType = valueType;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public DBConnectorParamType setDescription(String description) {
    this.description = description;
    return this;
  }

  public boolean isRequired() {
    return required;
  }

  public DBConnectorParamType setRequired(boolean required) {
    this.required = required;
    return this;
  }

  public boolean isSecret() {
    return secret;
  }

  public DBConnectorParamType setSecret(boolean secret) {
    this.secret = secret;
    return this;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public DBConnectorParamType setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }
}
