package eu.tsamek.dbviewer.services;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import eu.tsamek.dbviewer.api.DBConnectionParameter;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.entities.DBConnection;
import eu.tsamek.dbviewer.entities.DBConnector;
import eu.tsamek.dbviewer.entities.DBConnectorParamType;
import eu.tsamek.dbviewer.repository.ConnectionsRepository;
import eu.tsamek.dbviewer.repository.DriversRepository;
import eu.tsamek.dbviewer.utils.CryptoUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

@Service
public class ConnectionsService {

  private static final Logger logger = LoggerFactory.getLogger(ConnectionsService.class);

  private static final Pattern NAME_PATTERN = Pattern.compile("^[a-zA-Z0-9][a-zA-Z0-9_\\-]+$");

  private final ConnectionsRepository connectionsRepository;
  private final DriversRepository driversRepository;

  @Autowired
  public ConnectionsService(
          @Nonnull final ConnectionsRepository connectionsRepository,
          @Nonnull final DriversRepository driversRepository) {
    this.connectionsRepository = connectionsRepository;
    this.driversRepository = driversRepository;
  }

  public Flux<String> getAllConnections() {
    return Flux.fromIterable(connectionsRepository.findAll()).map(DBConnection::getName);
  }

  public Mono<DBConnection> getConnection(@Nonnull final String name) {
    return Mono.justOrEmpty(connectionsRepository.getByName(name)).map(this::obfuscateSecrets);
  }

  @Nonnull
  private DBConnection obfuscateSecrets(@Nonnull final DBConnection connection) {
    final var driver = driversRepository.findFirstByType(connection.getType());
    if (driver == null) {
      throw new DBFormatException("There is no driver for connection type " + connection.getType() + "!");
    }
    final var props = Maps.newLinkedHashMap(connection.getProperties());

    Flux.fromIterable(driver.getProperties())
        .filter(DBConnectorParamType::isSecret)
        .doOnNext(dbConnectorParamType -> props.computeIfPresent(dbConnectorParamType.getName(), (s, s2) -> "*****"))
        .blockLast();

    return connection.setProperties(props);
  }

  @Nonnull
  public Mono<DBConnection> storeConnection(@Nonnull final DBConnection connection) {

    final Set<String> validation = Sets.newHashSet();

    validateConnection(connection, validation);

    if (!validation.isEmpty()) {
      validation.forEach(message -> logger.warn("Validation error: {}", message));
      return Mono.empty();
    }

    final var driver = driversRepository.findFirstByType(connection.getType());

    fixDefaults(driver, connection);
    encodeSecrets(driver, connection);

    connection.setId(UUID.randomUUID());
    return Mono.just(connectionsRepository.save(connection)).map(this::obfuscateSecrets);
  }

  private void fixDefaults(@Nonnull final DBConnector driver, @Nonnull final DBConnection connection) {

    Flux.fromIterable(driver.getProperties())
        .filter(dbDriverParamType -> Objects.nonNull(dbDriverParamType.getDefaultValue()))
        .doOnNext(dbDriverParamType -> fixProperty(connection, dbDriverParamType))
        .blockLast();
  }

  @Nonnull
  private String fixProperty(
          @Nonnull final DBConnection connection, @Nonnull final DBConnectorParamType dbDriverParamType) {
    return connection.getProperties().putIfAbsent(dbDriverParamType.getName(), dbDriverParamType.getDefaultValue());
  }

  private void encodeSecrets(@Nonnull final DBConnector driver, @Nonnull final DBConnection connection) {
    Flux.fromIterable(driver.getProperties())
        .filter(DBConnectorParamType::isSecret)
        .doOnNext(dbDriverParamType -> encodeParameter(connection, dbDriverParamType))
        .blockLast();
  }

  @Nonnull
  private String encodeParameter(
          @Nonnull final DBConnection connection, @Nonnull final DBConnectorParamType dbDriverParamType) {
    return connection.getProperties().computeIfPresent(dbDriverParamType.getName(), (key, value) -> {
      try {
        return CryptoUtils.encrypt(value);
      } catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      return value;
    });
  }

  public void validateConnection(
          @Nonnull final DBConnection connection, @Nonnull final Collection<String> validation) {

    final var driver = driversRepository.findFirstByType(connection.getType());

    validateType(driver, connection.getType(), validation);
    validateName(driver, connection.getName(), validation);
    validateProperties(driver, connection.getProperties(), validation);
  }

  public void deleteConnection(@Nonnull final String name) {

    final var dbConnection = connectionsRepository.getByName(name);
    if (Objects.nonNull(dbConnection)) {
      connectionsRepository.delete(dbConnection);
    }
  }

  private void validateName(
          @Nonnull final DBConnector driver,
          @Nullable final String name,
          @Nonnull final Collection<String> description) {
    if (Objects.isNull(name)) {
      description.add("Name can not be null!");
    } else if (name.isEmpty()) {
      description.add("Name can not be empty!");
    } else if (!NAME_PATTERN.matcher(name).matches()) {
      description.add("Name has to follow this pattern: " + NAME_PATTERN);
    }
  }

  private void validateType(
          @Nonnull final DBConnector driver,
          @Nonnull final String type,
          @Nonnull final Collection<String> description) {
    if (Objects.isNull(driver)) {
      description.add("Database type " + type + " is not supported.");
    }
  }

  private void validateProperties(
          @Nonnull final DBConnector driver,
          @Nonnull final Map<String, String> properties,
          @Nonnull final Collection<String> description) {

    driver.getProperties().forEach(dbDriverParamType -> {
      final var name = dbDriverParamType.getName();
      final var value = properties.getOrDefault(name, dbDriverParamType.getDefaultValue());
      if (dbDriverParamType.isRequired() && StringUtils.isEmpty(value)) {
        description.add("Missing required property: " + name);
      }

      if (DBConnectionParameter.DataType.INTEGER == DBConnectionParameter.DataType.valueOf(dbDriverParamType.getValueType())) {
        try {
          Integer.parseInt(value);
        } catch (NumberFormatException e) {
          description.add("Invalid number format of property: " + name);
        }
      }
    });
  }
}
