package eu.tsamek.dbviewer.services;

import eu.tsamek.dbviewer.api.DBConnectionParameter;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.api.DBService;
import eu.tsamek.dbviewer.entities.DBConnector;
import eu.tsamek.dbviewer.entities.DBConnectorParamType;
import eu.tsamek.dbviewer.repository.DriversRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.ServiceLoader;
import java.util.UUID;

@Service
public class DriversService implements InitializingBean {

  private final DriversRepository driversRepository;

  @Autowired
  public DriversService(@Nonnull final DriversRepository driversRepository) {
    this.driversRepository = driversRepository;
  }

  @Override
  public void afterPropertiesSet() {
    final var loader = ServiceLoader.load(DBService.class);

    driversRepository.deleteAll();

    driversRepository.save(Flux.fromIterable(loader).map(this::convertConnector).toIterable());
  }

  @Nonnull
  public Flux<String> getSupportedDatabases() {
    return Flux.fromIterable(driversRepository.findAll()).map(DBConnector::getType).distinct();
  }

  @Nonnull
  public Mono<DBConnector> getDriver(@Nonnull final String type) {
    return Mono.justOrEmpty(driversRepository.findFirstByType(type));
  }

  @Nonnull
  private DBConnector convertConnector(@Nonnull final DBService dbService) {
    return new DBConnector().setId(UUID.randomUUID())
                            .setType(dbService.databaseType())
                            .setClassName(dbService.getClass().getName())
                            .setVersion(dbService.version())
                            .setObjects(extractSortedDbObjectNames(dbService))
                            .setProperties(extractSupportedProperties(dbService));
  }

  @Nonnull
  private List<String> extractSortedDbObjectNames(@Nonnull final DBService dbService) {
    return Flux.fromIterable(dbService.supportedDbObjects()).map(DBObject::name).collectList().block();
  }

  @Nonnull
  private List<DBConnectorParamType> extractSupportedProperties(@Nonnull final DBService dbService) {
    return Flux.fromIterable(dbService.supportedParameters()).map(this::convertConnectionParam).collectList().block();
  }

  @Nonnull
  private DBConnectorParamType convertConnectionParam(@Nonnull final DBConnectionParameter connectionParameter) {
    return new DBConnectorParamType().setName(connectionParameter.getName())
                                     .setValueType(connectionParameter.getDataType().name())
                                     .setDefaultValue(connectionParameter.getDefaultValue())
                                     .setDescription(connectionParameter.getDescription())
                                     .setRequired(connectionParameter.isRequired())
                                     .setSecret(connectionParameter.isSecret());
  }
}
