package eu.tsamek.dbviewer.services;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.api.DBNotFoundException;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.api.DBObjectPath;
import eu.tsamek.dbviewer.api.DBService;
import eu.tsamek.dbviewer.entities.DBConnection;
import eu.tsamek.dbviewer.entities.DBConnector;
import eu.tsamek.dbviewer.entities.DBConnectorParamType;
import eu.tsamek.dbviewer.repository.ConnectionsRepository;
import eu.tsamek.dbviewer.repository.DriversRepository;
import eu.tsamek.dbviewer.utils.CryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.function.Function;


@Service
public class ExecutionService {

  private static final Logger logger = LoggerFactory.getLogger(ExecutionService.class);

  private final ConnectionsRepository connectionsRepository;
  private final DriversRepository driversRepository;

  @Autowired
  public ExecutionService(
          @Nonnull final ConnectionsRepository connectionsRepository,
          @Nonnull final DriversRepository driversRepository) {
    this.connectionsRepository = connectionsRepository;
    this.driversRepository = driversRepository;
  }

  @Nonnull
  public Mono<DBDescription> describe(@Nonnull final String connection) {
    return describe(connection, null);
  }

  @Nonnull
  public Mono<DBDescription> describe(@Nonnull final String connection, @Nullable final String objectPath) {
    return describe(connection, objectPath, Collections.emptyMap());
  }

  @Nonnull
  public Mono<DBDescription> describe(
          @Nonnull final String connection,
          @Nullable final String objectPath,
          @Nonnull final Map<String, String> options) {
    return execute(
            connection,
            descriptor -> DBConnectorContext.constructForPath(resolvePath(descriptor, objectPath)).withOptions(options),
            DBService::describe);
  }

  @Nonnull
  public Mono<DBDescription> preview(@Nonnull final String connection) {
    return preview(connection, null);
  }

  @Nonnull
  public Mono<DBDescription> preview(@Nonnull final String connection, @Nullable final String objectPath) {
    return preview(connection, objectPath, Collections.emptyMap());
  }

  @Nonnull
  public Mono<DBDescription> preview(
          @Nonnull final String connection,
          @Nullable final String objectPath,
          @Nonnull final Map<String, String> options) {
    return execute(
            connection,
            descriptor -> DBConnectorContext.constructForPath(resolvePath(descriptor, objectPath)).withOptions(options),
            DBService::preview);
  }

  @Nonnull
  public Mono<DBDescription> statistic(@Nonnull final String connection) {
    return preview(connection, null);
  }

  @Nonnull
  public Mono<DBDescription> statistic(@Nonnull final String connection, @Nullable final String objectPath) {
    return preview(connection, objectPath, Collections.emptyMap());
  }

  @Nonnull
  public Mono<DBDescription> statistic(
          @Nonnull final String connection,
          @Nullable final String objectPath,
          @Nonnull final Map<String, String> options) {
    return execute(
            connection,
            descriptor -> DBConnectorContext.constructForPath(resolvePath(descriptor, objectPath)).withOptions(options),
            DBService::statistic);
  }

  @Nonnull
  public Mono<DBDescription> query(@Nonnull final String connection, @Nullable final String query) {
    return execute(connection, descriptor -> DBConnectorContext.constructForQuery(query), DBService::query);
  }

  private Mono<DBDescription> execute(
          @Nonnull final String connection,
          @Nonnull final Function<DBService, DBConnectorContext.Builder> contextBuilderSupplier,
          @Nonnull final ExecutionFunction<DBService, DBConnectorContext, Mono<? extends DBDescription>> executionFunction) {

    final var dbConnection = connectionsRepository.getByName(connection);
    if (dbConnection == null) {
      throw new DBNotFoundException("There is no connection with name " + connection + "!");
    }

    final var driver = driversRepository.findFirstByType(dbConnection.getType());
    if (driver == null) {
      throw new DBFormatException("There is no driver for connection type " + dbConnection.getType() + "!");
    }

    decodeSecrets(driver, dbConnection);

    try {
      final var descriptor = (DBService) Class.forName(driver.getClassName()).getDeclaredConstructor().newInstance();

      return Mono.using(
              descriptor::init,
              desc -> executionFunction.apply(desc,
                      contextBuilderSupplier.apply(descriptor).withParameters(dbConnection.getProperties()).build()),
              DBService::destroy);
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private DBObjectPath resolvePath(@Nonnull final DBService descriptor, @Nullable final String objectQuery) {

    if (objectQuery == null) {
      return new DBObjectPath();
    }

    var supportedObjects = translateToMap(descriptor.rootDbObjects());
    final var objects = new DBObjectPath();

    final var stringTokenizer = new StringTokenizer(objectQuery, "/");

    DBObject objectType = null;
    String objectName = null;
    while (stringTokenizer.hasMoreTokens()) {
      if (Objects.isNull(objectType)) {
        var objectTypeName = stringTokenizer.nextToken();
        objectType = supportedObjects.get(objectTypeName.toLowerCase());
        if (objectType == null) {
          throw new DBFormatException("Unexpected object type " + objectTypeName + "!");
        }
        supportedObjects = translateToMap(objectType.next());
      } else {
        objectName = stringTokenizer.nextToken();
      }

      if (Objects.nonNull(objectName)) {
        objects.put(objectType, objectName);
        objectType = null;
        objectName = null;
      }
    }

    if (Objects.nonNull(objectType)) {
      objects.put(objectType, "*");
    }

    return objects;
  }

  private void decodeSecrets(@Nonnull final DBConnector driver, @Nonnull final DBConnection connection) {
    Flux.fromIterable(driver.getProperties())
        .filter(DBConnectorParamType::isSecret)
        .doOnNext(dbDriverParamType -> decodeParameter(connection, dbDriverParamType))
        .blockLast();
  }

  @Nonnull
  private String decodeParameter(
          @Nonnull final DBConnection connection, @Nonnull final DBConnectorParamType dbDriverParamType) {
    return connection.getProperties().computeIfPresent(dbDriverParamType.getName(), (key, value) -> {
      try {
        return CryptoUtils.decrypt(value);
      } catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      return value;
    });
  }

  @Nonnull
  private Map<String, ? extends DBObject> translateToMap(@Nonnull final Collection<? extends DBObject> objects) {
    return Flux.fromIterable(objects).collectMap(dbObject -> dbObject.name().toLowerCase()).block();
  }

  @FunctionalInterface
  private interface ExecutionFunction<A, B, R> {
    R apply(A a, B b);
  }
}
