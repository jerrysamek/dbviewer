package eu.tsamek.dbviewer.repository;

import eu.tsamek.dbviewer.entities.DBConnector;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DriversRepository extends CassandraRepository<DBConnector> {

  DBConnector findFirstByType(String type);
}
