package eu.tsamek.dbviewer.repository;

import eu.tsamek.dbviewer.entities.DBConnection;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionsRepository extends CassandraRepository<DBConnection> {

  DBConnection getByName(String name);
}
