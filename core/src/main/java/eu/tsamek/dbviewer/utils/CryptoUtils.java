package eu.tsamek.dbviewer.utils;

import org.apache.commons.crypto.cipher.CryptoCipher;
import org.apache.commons.crypto.cipher.CryptoCipherFactory;
import org.apache.commons.crypto.utils.Utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;

public final class CryptoUtils {

  private static final SecretKeySpec SECRET_KEY_SPEC = new SecretKeySpec(getUTF8Bytes("CryptoUtils.123*"), "AES");
  private static final IvParameterSpec IV_PARAMETER_SPEC = new IvParameterSpec(getUTF8Bytes("CryptoUtils.123*"));
  private static final String TRANSFORM = "AES/CBC/PKCS5Padding";

  private CryptoUtils() {
  }

  public static String encrypt(String sampleInput) throws InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, ShortBufferException, IllegalBlockSizeException, IOException {
    Properties properties = new Properties();
    properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.JCE.getClassName());
    //Creates a CryptoCipher instance with the transformation and properties.

    CryptoCipher encipher = Utils.getCipherInstance(TRANSFORM, properties);

    byte[] input = getUTF8Bytes(sampleInput);
    byte[] output = new byte[1024];

    //Initializes the cipher with ENCRYPT_MODE, SECRET_KEY_SPEC and IV_PARAMETER_SPEC.
    encipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY_SPEC, IV_PARAMETER_SPEC);
    //Continues a multiple-part encryption/decryption operation for byte array.
    final int updateBytes = encipher.update(input, 0, input.length, output, 0);
    //We must call doFinal at the end of encryption/decryption.
    final int finalBytes = encipher.doFinal(input, 0, 0, output, updateBytes);

    //Closes the cipher.
    encipher.close();

    return new String(Base64.getEncoder().encode(Arrays.copyOf(output, updateBytes + finalBytes)));
  }

  public static String decrypt(String sampleInput) throws IOException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, ShortBufferException, IllegalBlockSizeException {
    Properties properties = new Properties();
    properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.JCE.getClassName());

    CryptoCipher decipher = Utils.getCipherInstance(TRANSFORM, properties);

    decipher.init(Cipher.DECRYPT_MODE, SECRET_KEY_SPEC, IV_PARAMETER_SPEC);
    byte[] input = Base64.getDecoder().decode(getUTF8Bytes(sampleInput));

    byte[] decoded = new byte[input.length];
    int finalBytes = decipher.doFinal(input, 0, input.length, decoded, 0);

    return new String(Arrays.copyOf(decoded, finalBytes), StandardCharsets.UTF_8);
  }

  private static byte[] getUTF8Bytes(String input) {
    return input.getBytes(StandardCharsets.UTF_8);
  }
}
