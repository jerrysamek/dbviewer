package eu.tsamek.dbviewer;

import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DBFormatExceptionHandler implements ExceptionMapper<DBFormatException> {
  private static final Logger logger = LoggerFactory.getLogger(DBFormatExceptionHandler.class);

  @Override
  public Response toResponse(final DBFormatException exception) {
    logger.debug(exception.getMessage(), exception);
    return Response.status(Response.Status.BAD_REQUEST)
                   .entity(ErrorMessage.wrap(exception.getMessage()))
                   .type(MediaType.APPLICATION_JSON_TYPE)
                   .build();
  }
}
