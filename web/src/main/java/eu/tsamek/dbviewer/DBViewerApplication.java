package eu.tsamek.dbviewer;

import com.google.common.collect.Sets;
import eu.tsamek.dbviewer.endpoints.Connections;
import eu.tsamek.dbviewer.endpoints.Databases;
import eu.tsamek.dbviewer.endpoints.Descriptions;
import eu.tsamek.dbviewer.endpoints.Previews;
import eu.tsamek.dbviewer.endpoints.Statistics;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("/dbviewer")
public class DBViewerApplication extends Application {

  public Set<Class<?>> getClasses() {
    return Sets.newHashSet(
            Databases.class,
            Connections.class,
            Descriptions.class,
            Statistics.class,
            Previews.class,
            ObjectMapperProvider.class,
            DBFormatExceptionHandler.class,
            DBNotFoundExceptionHandler.class,
            NotFoundExceptionHandler.class,
            GenericExceptionHandler.class);
  }
}
