package eu.tsamek.dbviewer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import eu.tsamek.dbviewer.api.DBConnectorException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author Tomas Samek
 * <p>
 * Small utility that allows Flux, Mono and Stream objects to be serialized directly into the response writer.
 */
@Provider
public class ObjectMapperProvider implements ContextResolver<ObjectMapper> {

  private final ObjectMapper defaultObjectMapper;

  public ObjectMapperProvider() {
    defaultObjectMapper = createDefaultMapper();
  }

  private static ObjectMapper createDefaultMapper() {
    final var mapper = new ObjectMapper();

    mapper.disable(SerializationFeature.INDENT_OUTPUT);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    mapper.registerModule(new ModernModule());

    return mapper;
  }

  private static void arrayStartSafe(@Nonnull final JsonGenerator gen) {
    try {
      gen.writeStartArray();
    } catch (IOException e) {
      throw new DBConnectorException(e);
    }
  }

  private static void arrayEndSafe(@Nonnull final JsonGenerator gen) {
    try {
      gen.writeEndArray();
    } catch (IOException e) {
      throw new DBConnectorException(e);
    }
  }

  private static void writeSafe(@Nonnull final JsonGenerator gen, @Nullable final Object object) {
    try {
      gen.writeObject(object);
    } catch (Exception e) {
      throw new DBConnectorException(e);
    }
  }

  @Override
  public ObjectMapper getContext(final Class<?> type) {
    return defaultObjectMapper;
  }

  private static class ModernModule extends SimpleModule {
    public ModernModule() {
      addSerializer(new MonoSerializer());
      addSerializer(new FluxSerializer());
    }
  }

  private static class MonoSerializer extends JsonSerializer<Mono> {
    @Nonnull
    @Override
    public Class<Mono> handledType() {
      return Mono.class;
    }

    @Override
    public void serialize(
            @Nonnull final Mono value,
            @Nonnull final JsonGenerator gen,
            @Nonnull final SerializerProvider serializers) throws IOException {
      try {
        writeSafe(gen, value.block());
      } catch (DBConnectorException e) {
        throw new IOException(e);
      }
    }
  }

  private static class FluxSerializer extends JsonSerializer<Flux> {

    @Nonnull
    @Override
    public Class<Flux> handledType() {
      return Flux.class;
    }

    @Override
    public void serialize(
            @Nonnull final Flux value,
            @Nonnull final JsonGenerator gen,
            @Nonnull final SerializerProvider serializers) throws IOException {
      try {
        value.doOnSubscribe(subscription -> arrayStartSafe(gen))
             .doOnNext(object -> writeSafe(gen, object))
             .doOnComplete(() -> arrayEndSafe(gen))
             .blockLast();
      } catch (DBConnectorException e) {
        throw new IOException(e);
      }
    }
  }
}