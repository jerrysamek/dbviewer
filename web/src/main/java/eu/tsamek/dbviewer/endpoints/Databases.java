package eu.tsamek.dbviewer.endpoints;

import eu.tsamek.dbviewer.beans.DatabaseDetail;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import eu.tsamek.dbviewer.services.DriversService;
import eu.tsamek.dbviewer.utils.ResponseHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api(description = "Databases")
@Path("databases")
public class Databases {

  @Inject
  private DriversService driversService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Returns list of all database names this application is able to connect to.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = List.class)
  })
  public Response getSupportedDatabases() {
    return Response.ok(driversService.getSupportedDatabases()).build();
  }

  @GET
  @Path("{dbType}")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Returns database detail for given dbType path parameter. This detail also contains information about all connection parameters required to create db connection.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = DatabaseDetail.class),
          @ApiResponse(code = 404, message = "database for given type does not exist", response = ErrorMessage.class)
  })
  public Response getDatabaseDetails(@ApiParam("identifier of database type (e.g. MySQL)") @Nonnull final @PathParam("dbType") String dbType) {
    return ResponseHandler.okOrNotFound(driversService.getDriver(dbType).map(DatabaseDetail::from),
            "Database doesn't exist!");
  }
}
