package eu.tsamek.dbviewer.endpoints;

import eu.tsamek.dbviewer.beans.DescriptionResult;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import eu.tsamek.dbviewer.services.ExecutionService;
import eu.tsamek.dbviewer.utils.OptionsParser;
import eu.tsamek.dbviewer.utils.ResponseHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(description = "Statistics")
@Path("connections/{connectionName}/statistics")
public class Statistics {

  @Inject
  private ExecutionService executionService;

  @GET
  @Path("{object : .+}")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Returns statistics for given connectionName path parameter.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = DescriptionResult.class),
          @ApiResponse(code = 400, message = "connection is invalid, object query is not resolvable by db service or statistics can not be made for given object", response = ErrorMessage.class),
          @ApiResponse(code = 404, message = "requested object name doesn't exist", response = ErrorMessage.class)
  })
  public Response statistic(
          @ApiParam("name of connection") @Nonnull final @PathParam("connectionName") String connection,
          @ApiParam(value = "query in format /object/value e.g. /schema/myschema/table/mytable", example = "/schema/myschema/table/mytable") @Nonnull final @PathParam("object") String objectQuery,
          @ApiParam("additional options in format options=key1:val1;key2:val2") @Nullable final @QueryParam("options") String options) {
    return ResponseHandler.okOrNotFound(
            executionService.statistic(connection, objectQuery, OptionsParser.parseOptions(options))
                            .map(DescriptionResult::from),
            "Object for query doesn't exist!");

  }
}
