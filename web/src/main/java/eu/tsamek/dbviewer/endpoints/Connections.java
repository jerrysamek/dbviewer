package eu.tsamek.dbviewer.endpoints;

import com.google.common.collect.Sets;
import eu.tsamek.dbviewer.beans.ConnectionDescription;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import eu.tsamek.dbviewer.entities.DBConnection;
import eu.tsamek.dbviewer.services.ConnectionsService;
import eu.tsamek.dbviewer.utils.ResponseHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Path("connections")
@Api(description = "Connections")
public class Connections {

  @Inject
  private ConnectionsService connectionsService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Returns name of all stored connections.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = List.class)
  })
  public Response getAllConnections() {
    return Response.ok(connectionsService.getAllConnections()).build();
  }

  @GET
  @Path("{connectionName}")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Returns details for given connectionName path parameter.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = ConnectionDescription.class),
          @ApiResponse(code = 404, message = "connection with given name not found", response = ErrorMessage.class)
  })
  public Response getConnection(@ApiParam("name of connection to return") @Nonnull final @PathParam("connectionName") String connectionName) {
    return ResponseHandler.okOrNotFound(connectionsService.getConnection(connectionName)
                                                          .map(ConnectionDescription::from)
                                                          .filter(Objects::nonNull), "Connection does not exist!");
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Adds new connection details. Connection details have to contain name and existing type of db. Also all required connection details for given db type in form of map.", notes = "payload example:\n{\n  \"name\":\"example\",\n  \"type\":\"MySQL\",\n  \"connectionDetails\":\n  {\n    \"host\":\"127.0.0.1\",\n    \"dbname\":\"sys\",\n    \"username\":\"user\",\n    \"password\":\"password\",\n    \"port\":\"3306\"\n  }\n}")
  @ApiResponses({
          @ApiResponse(code = 201, message = "successful operation", response = ConnectionDescription.class),
          @ApiResponse(code = 400, message = "connection parameters are invalid", response = ErrorMessage.class),
          @ApiResponse(code = 409, message = "connection with given name already exists", response = ErrorMessage.class)
  })
  public Response addConnection(@ApiParam(name = "payload", value = "json object with name of the connection, database type and database connections parameters", format = "application/json", required = true) @Nonnull final ConnectionDescription description) {

    if (connectionExists(description.getName())) {
      return Response.status(Response.Status.CONFLICT).entity(ErrorMessage.wrap("Already exists!")).build();
    }

    final DBConnection dbConnection = new DBConnection().setName(description.getName())
                                                        .setType(description.getType())
                                                        .setProperties(description.getConnectionDetails());
    final Set<String> validation = Sets.newHashSet();
    connectionsService.validateConnection(dbConnection, validation);

    if (!validation.isEmpty()) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ErrorMessage.wrap(validation)).build();
    }

    return Response.created(URI.create("connections/" + description.getName()))
                   .entity(connectionsService.storeConnection(dbConnection).map(ConnectionDescription::from))
                   .build();

  }

  @DELETE
  @Path("{connectionName}")
  @ApiOperation(value = "Removes connection details.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation"),
          @ApiResponse(code = 404, message = "connection with given name not found", response = ErrorMessage.class),
          @ApiResponse(code = 500, message = "deletion wasn't successful", response = ErrorMessage.class)
  })
  public Response deleteConnection(@ApiParam("name of connection to delete") @Nonnull final @PathParam("connectionName") String connectionName) {

    if (!connectionExists(connectionName)) {
      return getNotFoundResponseBuilder("Connection does not exist!").block().build();
    }

    connectionsService.deleteConnection(connectionName);

    return connectionsService.getConnection(connectionName)
                             .blockOptional()
                             .map(dbConnection -> getErrorResponseBuilder(
                                     "ConnectionDescriptor wasn't deleted for unknown reason!"))
                             .orElse(Response.ok())
                             .build();
  }

  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{connectionName}")
  @ApiOperation(value = "Updates stored connection details based.", notes = "payload example:\n{\n  \"name\":\"example\",\n  \"type\":\"MySQL\",\n  \"connectionDetails\":\n  {\n    \"host\":\"127.0.0.1\",\n    \"dbname\":\"sys\",\n    \"username\":\"user\",\n    \"password\":\"password\",\n    \"port\":\"3306\"\n  }\n}")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = ConnectionDescription.class),
          @ApiResponse(code = 400, message = "connection parameters are invalid", response = ErrorMessage.class),
          @ApiResponse(code = 404, message = "connection with given name not found", response = ErrorMessage.class)
  })
  public Response updateConnection(
          @ApiParam("name of connection to update") @Nonnull final @PathParam("connectionName") String connectionName,
          @ApiParam(name = "payload", value = "json object with name of the connection, database type and database connections parameters", format = "application/json", required = true) @Nonnull final ConnectionDescription description) {

    if (!connectionExists(connectionName)) {
      return getNotFoundResponseBuilder("Connection does not exist!").block().build();
    }

    final var dbConnection = new DBConnection().setName(description.getName())
                                               .setType(description.getType())
                                               .setProperties(description.getConnectionDetails());
    final Set<String> validation = Sets.newHashSet();
    connectionsService.validateConnection(dbConnection, validation);

    if (!validation.isEmpty()) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ErrorMessage.wrap(validation)).build();
    }

    connectionsService.deleteConnection(connectionName);

    return Response.ok(connectionsService.storeConnection(dbConnection).map(ConnectionDescription::from)).build();
  }

  @Nonnull
  private Mono<Response.ResponseBuilder> getNotFoundResponseBuilder(@Nonnull final String message) {
    return Mono.defer(() -> Mono.just(Response.status(Response.Status.NOT_FOUND).entity(ErrorMessage.wrap(message))));
  }

  @Nonnull
  private Response.ResponseBuilder getErrorResponseBuilder(@Nonnull final String message) {
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ErrorMessage.wrap(message));
  }

  private boolean connectionExists(@Nonnull final String connectionName) {
    return connectionsService.getConnection(connectionName).blockOptional().isPresent();
  }
}
