package eu.tsamek.dbviewer.endpoints;

import eu.tsamek.dbviewer.beans.DescriptionResult;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import eu.tsamek.dbviewer.services.ExecutionService;
import eu.tsamek.dbviewer.utils.OptionsParser;
import eu.tsamek.dbviewer.utils.ResponseHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(description = "Descriptions")
@Path("connections/{connectionName}/descriptions")
public class Descriptions {

  @Inject
  private ExecutionService executionService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Describes current connection to database and returns all information about it.")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = DescriptionResult.class),
          @ApiResponse(code = 400, message = "connection is invalid", response = ErrorMessage.class),
          @ApiResponse(code = 404, message = "connection name doesn't exist", response = ErrorMessage.class)
  })
  public Response describe(@ApiParam("name of connection") @Nonnull final @PathParam("connectionName") String connection) {
    return ResponseHandler.ok(executionService.describe(connection).map(DescriptionResult::from));
  }

  @GET
  @Path("{object : .+}")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Describes object specified by object query. Query consists of database object type and its name. For example table/mytable where table is type of object that should be described and mytable is name of the object that should be described. This query can ends with both object value and object type. When it ends with object type all objects are listed.\nThis endpoint accepts options includeSystem parameter which allows to list system objects by specifying query parameter options.", notes = "query example1: /schema/myschema/table/mytable\nquery example2: /schema/myschema/table\nquery example3: /schema/myschema/table?options=includeSystem:true")
  @ApiResponses({
          @ApiResponse(code = 200, message = "successful operation", response = DescriptionResult.class),
          @ApiResponse(code = 400, message = "connection is invalid, object query is not resolvable by db service or description can not be made for given object", response = ErrorMessage.class),
          @ApiResponse(code = 404, message = "connection name doesn't exist or requested object name doesn't exist", response = ErrorMessage.class)
  })
  public Response describe(
          @ApiParam("name of connection") @Nonnull final @PathParam("connectionName") String connection,
          @ApiParam(value = "query in format /object/value e.g. /schema/myschema/table/mytable", example = "/schema/myschema/table/mytable") @Nonnull final @PathParam("object") String objectQuery,
          @ApiParam("additional options in format ?options=key1:val1;key2:val2") @Nullable final @QueryParam("options") String options) {

    return ResponseHandler.okOrNotFound(executionService.describe(connection,
            objectQuery,
            OptionsParser.parseOptions(options)), "Leaf object for query doesn't exist!");
  }
}
