package eu.tsamek.dbviewer.beans;

import eu.tsamek.dbviewer.entities.DBConnector;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import reactor.core.publisher.Flux;

import javax.annotation.Nonnull;

@ApiModel(description = "Database detail.")
public class DatabaseDetail {

  @ApiModelProperty(dataType = "string")
  private String dbName;

  @ApiModelProperty(dataType = "java.util.ArrayList")
  private Flux<String> dbObjects = Flux.empty();

  @ApiModelProperty(dataType = "java.util.ArrayList")
  private Flux<ConnectionParameter> parameters = Flux.empty();

  @Nonnull
  public static DatabaseDetail from(@Nonnull final DBConnector dbConnector) {
    return new DatabaseDetail().setDbName(dbConnector.getType())
                               .setDbObjects(Flux.fromIterable(dbConnector.getObjects()).map(String::toLowerCase))
                               .setParameters(Flux.fromIterable(dbConnector.getProperties())
                                                  .map(ConnectionParameter::from));

  }

  public String getDbName() {
    return dbName;
  }

  public DatabaseDetail setDbName(String dbName) {
    this.dbName = dbName;
    return this;
  }

  public Flux<String> getDbObjects() {
    return dbObjects;
  }

  public DatabaseDetail setDbObjects(Flux<String> dbObjects) {
    this.dbObjects = dbObjects.cache();
    return this;
  }

  public Flux<ConnectionParameter> getParameters() {
    return parameters;
  }

  public DatabaseDetail setParameters(Flux<ConnectionParameter> parameters) {
    this.parameters = parameters.cache();
    return this;
  }
}
