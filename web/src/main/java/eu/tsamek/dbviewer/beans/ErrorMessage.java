package eu.tsamek.dbviewer.beans;

import io.swagger.annotations.ApiModel;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;

@ApiModel(description = "Generic error wrapper.")
public class ErrorMessage<E> {

  private Collection<E> errors = null;

  @Nonnull
  public static <T> ErrorMessage<T> wrap(@Nonnull final T message) {
    return wrap(Collections.singleton(message));
  }

  @Nonnull
  public static <T> ErrorMessage<T> wrap(@Nonnull final Collection<T> collection) {
    return new ErrorMessage<T>().setErrors(collection);
  }

  @Nonnull
  public Collection<E> getErrors() {
    return errors;
  }

  public ErrorMessage<E> setErrors(@Nonnull final Collection<E> errors) {
    this.errors = errors;
    return this;
  }
}
