package eu.tsamek.dbviewer.beans;

import eu.tsamek.dbviewer.entities.DBConnectorParamType;

import javax.annotation.Nonnull;

public class ConnectionParameter {

  private String name;
  private String dataType;
  private String description;
  private boolean required = true;
  private boolean secret = false;
  private String defaultValue;

  @Nonnull
  public static ConnectionParameter from(@Nonnull final DBConnectorParamType dbConnectorParamType) {
    return new ConnectionParameter().setDataType(dbConnectorParamType.getValueType())
                                    .setName(dbConnectorParamType.getName())
                                    .setDefaultValue(dbConnectorParamType.getDefaultValue())
                                    .setDescription(dbConnectorParamType.getDescription())
                                    .setRequired(dbConnectorParamType.isRequired())
                                    .setSecret(dbConnectorParamType.isSecret());
  }

  public String getName() {
    return name;
  }

  public ConnectionParameter setName(String name) {
    this.name = name;
    return this;
  }

  public String getDataType() {
    return dataType;
  }

  public ConnectionParameter setDataType(String dataType) {
    this.dataType = dataType;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public ConnectionParameter setDescription(String description) {
    this.description = description;
    return this;
  }

  public boolean isRequired() {
    return required;
  }

  public ConnectionParameter setRequired(boolean required) {
    this.required = required;
    return this;
  }

  public boolean isSecret() {
    return secret;
  }

  public ConnectionParameter setSecret(boolean secret) {
    this.secret = secret;
    return this;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public ConnectionParameter setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }
}
