package eu.tsamek.dbviewer.beans;

import eu.tsamek.dbviewer.entities.DBConnection;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;

@ApiModel(description = "Connection Detail.")
public class ConnectionDescription {

  @ApiModelProperty(required = true)
  private String name;

  @ApiModelProperty(required = true)
  private String type;

  @ApiModelProperty(required = true)
  private Map<String, String> connectionDetails = Collections.emptyMap();

  @Nonnull
  public static ConnectionDescription from(@Nonnull final DBConnection connection) {
    return new ConnectionDescription().setName(connection.getName())
                                      .setType(connection.getType())
                                      .setConnectionDetails(connection.getProperties());
  }

  public String getName() {
    return name;
  }

  public ConnectionDescription setName(String name) {
    this.name = name;
    return this;
  }

  public String getType() {
    return type;
  }

  public ConnectionDescription setType(String type) {
    this.type = type;
    return this;
  }

  public Map<String, String> getConnectionDetails() {
    return connectionDetails;
  }

  public ConnectionDescription setConnectionDetails(Map<String, String> connectionDetails) {
    this.connectionDetails = connectionDetails;
    return this;
  }
}
