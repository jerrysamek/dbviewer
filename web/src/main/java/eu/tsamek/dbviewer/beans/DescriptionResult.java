package eu.tsamek.dbviewer.beans;

import eu.tsamek.dbviewer.api.DBDescription;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import reactor.core.publisher.Flux;

import javax.annotation.Nonnull;
import java.util.Map;

@ApiModel(description = "Result of current service.")
public class DescriptionResult {

  @ApiModelProperty(dataType = "string")
  private String name;

  @ApiModelProperty(dataType = "string")
  private String description;

  @ApiModelProperty(dataType = "java.util.ArrayList")
  private Flux<Object> items;

  @ApiModelProperty(dataType = "java.util.HashMap")
  private Map<String, Object> parameters;

  @Nonnull
  public static DescriptionResult from(@Nonnull final DBDescription description) {
    return new DescriptionResult().setName(description.getName())
                                  .setDescription(description.getDescription())
                                  .setItems(description.getItems())
                                  .setParameters(description.getParameters());
  }

  public String getName() {
    return name;
  }

  public DescriptionResult setName(String name) {
    this.name = name;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public DescriptionResult setDescription(String description) {
    this.description = description;
    return this;
  }

  public Flux<Object> getItems() {
    return items;
  }

  public DescriptionResult setItems(Flux<Object> items) {
    this.items = items;
    return this;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public DescriptionResult setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
    return this;
  }
}
