package eu.tsamek.dbviewer.utils;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBFormatException;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;
import java.util.StringTokenizer;

public final class OptionsParser {

  private OptionsParser() {
  }

  @Nonnull
  public static Map<String, String> parseOptions(@Nullable final String optionsString) {
    if (StringUtils.isEmpty(optionsString)) {
      return Collections.emptyMap();
    }

    final StringTokenizer tokenizer = new StringTokenizer(optionsString, ";");

    final Map<String, String> options = Maps.newHashMap();
    while (tokenizer.hasMoreTokens()) {

      final var option = tokenizer.nextToken();
      final var parts = option.split(":");
      if (parts.length != 2) {
        throw new DBFormatException(
                "Wrong format of options query parameter. Usage ?options=option1:value1;option2:value2");
      }
      options.put(parts[0], parts[1]);
    }

    return options;
  }
}
