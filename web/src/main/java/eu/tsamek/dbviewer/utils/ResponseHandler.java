package eu.tsamek.dbviewer.utils;

import eu.tsamek.dbviewer.beans.ErrorMessage;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

public final class ResponseHandler {

  private ResponseHandler() {
  }

  @Nonnull
  public static Response ok(@Nonnull final Mono<?> mono) {
    return mono.map(Response::ok).map(Response.ResponseBuilder::build).block();
  }

  @Nonnull
  public static Response okOrNotFound(@Nonnull final Mono<?> mono, @Nonnull final String notFoundMessage) {

    return mono.map(Response::ok)
               .switchIfEmpty(Mono.defer(() -> Mono.just(Response.status(404)
                                                                 .entity(ErrorMessage.wrap(notFoundMessage)))))
               .map(Response.ResponseBuilder::build)
               .block();
  }
}
