package eu.tsamek.dbviewer;

import eu.tsamek.dbviewer.api.DBNotFoundException;
import eu.tsamek.dbviewer.beans.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DBNotFoundExceptionHandler implements ExceptionMapper<DBNotFoundException> {
  private static final Logger logger = LoggerFactory.getLogger(DBNotFoundExceptionHandler.class);

  @Override
  public Response toResponse(final DBNotFoundException exception) {
    logger.debug(exception.getMessage(), exception);
    return Response.status(Response.Status.NOT_FOUND)
                   .entity(ErrorMessage.wrap(exception.getMessage()))
                   .type(MediaType.APPLICATION_JSON_TYPE)
                   .build();
  }
}
