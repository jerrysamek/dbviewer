package eu.tsamek.dbviewer;

import eu.tsamek.dbviewer.beans.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

  private static final Logger logger = LoggerFactory.getLogger(NotFoundExceptionHandler.class);

  @Override
  public Response toResponse(final NotFoundException exception) {
    logger.info(exception.getMessage(), exception);
    return Response.status(Response.Status.NOT_FOUND)
                   .entity(ErrorMessage.wrap("Path doesn't exist!"))
                   .type(MediaType.APPLICATION_JSON_TYPE)
                   .build();
  }
}
