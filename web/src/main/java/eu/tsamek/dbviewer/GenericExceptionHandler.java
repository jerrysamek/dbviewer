package eu.tsamek.dbviewer;

import eu.tsamek.dbviewer.beans.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionHandler implements ExceptionMapper<Throwable> {

  private static final Logger logger = LoggerFactory.getLogger(GenericExceptionHandler.class);

  @Override
  public Response toResponse(final Throwable exception) {
    logger.error(exception.getMessage(), exception);
    return Response.serverError()
                   .entity(ErrorMessage.wrap("Internal server error!"))
                   .type(MediaType.APPLICATION_JSON_TYPE)
                   .build();
  }
}
