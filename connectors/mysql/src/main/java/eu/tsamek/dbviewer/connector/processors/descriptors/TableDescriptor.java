package eu.tsamek.dbviewer.connector.processors.descriptors;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.MySqlService;
import eu.tsamek.dbviewer.connector.beans.Table;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Map;

public class TableDescriptor implements NestedDescriptor {

  @Nonnull
  @Override
  public Mono<DBDescription> describe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    final var schema = extractSchema(connector, path);
    final var options = context.getOptions();
    final var leafObject = (MySqlService.MySQLObjects) path.getLeafObject();
    final var leafObjectValue = path.get(leafObject);

    validateSchema(schema);
    if ("*".equals(leafObjectValue)) {

      final var tableNames = getTables(connector, schema, tableTypes(leafObject.name(), shouldIncludeSystem(options)));

      return Mono.just(new DBDescription().setName("tables in " + formatObjectNames(schema))
                                          .setDescription("List of tables in schema " + formatObjectNames(schema) + ".")
                                          .setItems(tableNames));
    } else {

      return getTable(connector,
              schema,
              leafObjectValue,
              tableTypes(leafObject.name(), shouldIncludeSystem(options))).map(tableProperties -> {

        final Map<String, Object> properties = Maps.newLinkedHashMap();

        var primaryKeys = connector.getPrimaryKeys(schema, leafObjectValue).map(this::convertAllToMap);
        properties.put("primaryKeys", primaryKeys.cache());

        var exportedKeys = connector.getExportedKeys(schema, leafObjectValue).map(this::convertAllToMap);
        properties.put("referencedBy", exportedKeys.cache());

        var importedKeys = connector.getImportedKeys(schema, leafObjectValue).map(this::convertAllToMap);
        properties.put("foreignKeys", importedKeys.cache());

        var tablePrivileges = connector.getTablePrivileges(schema, leafObjectValue).map(this::convertAllToMap);
        properties.put("tablePrivileges", tablePrivileges.cache());

        return getDbDescription(schema, leafObject, leafObjectValue, properties);
      });
    }
  }

  @Nonnull
  private DBDescription getDbDescription(
          @Nonnull final String schema,
          @Nonnull final MySqlService.MySQLObjects leafObject,
          @Nonnull final String leafObjectValue,
          @Nonnull final Map<String, Object> properties) {
    return new DBDescription().setName("table " + formatObjectNames(schema, leafObjectValue))
                              .setDescription("Description of table " + formatObjectNames(schema,
                                      leafObjectValue) + ".")
                              .setParameters(properties)
                              .setItems(Flux.fromIterable(leafObject.next())
                                            .map(DBObject::name)
                                            .map(String::toLowerCase));
  }

  @Nonnull
  private Flux<Table> getTables(
          @Nonnull final MySqlConnector connector, @Nonnull final String schema, @Nonnull final String... tableTypes) {

    return connector.getAllTables(schema, tableTypes).map(resultSet -> {
      try {
        final var text = resultSet.getString("TABLE_NAME");
        final var type = resultSet.getString("TABLE_TYPE");
        final var realSchema = resultSet.getString("TABLE_CAT");

        return new Table().setName(text).setType(type).setSchema(realSchema);
      } catch (SQLException e) {
        throw new DBConnectorException(e);
      }
    });
  }

  @Nonnull
  private Mono<Map<String, Object>> getTable(
          @Nonnull final MySqlConnector connector,
          @Nonnull final String schema,
          @Nonnull final String table,
          @Nonnull final String... tableTypes) {

    return connector.getTable(schema, table, tableTypes).map(this::convertAllToMap);
  }

  private boolean shouldIncludeSystem(@Nonnull final Map<String, String> options) {
    return "true".equalsIgnoreCase(options.get("includeSystem"));
  }

  @Nonnull
  private String[] tableTypes(@Nonnull final String baseType, boolean includeSystem) {

    if (includeSystem) {
      return new String[]{baseType, "SYSTEM " + baseType};
    } else {
      return new String[]{baseType};
    }
  }
}
