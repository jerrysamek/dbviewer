package eu.tsamek.dbviewer.connector;

import eu.tsamek.dbviewer.api.DBConnectorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class MySqlConnector {
  private static final Logger logger = LoggerFactory.getLogger(MySqlConnector.class);

  static {
    if (logger.isDebugEnabled()) {
      DriverManager.setLogWriter(new PrintWriter(new DebugLogWriter(logger)));
    }

    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      throw new IllegalStateException("There is no mysql driver class available.");
    }
  }

  private final Connection realConnection;

  private final AtomicInteger openResults = new AtomicInteger(0);
  private final AtomicBoolean closed = new AtomicBoolean(false);

  public MySqlConnector(
          @Nonnull final String connectionString,
          @Nullable final String username,
          @Nullable final String password) throws SQLException {
    this.realConnection = DriverManager.getConnection(connectionString, username, password);
  }

  @Nonnull
  public DatabaseMetaData getMetaData() {
    try {
      return realConnection.getMetaData();
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nullable
  public String getCatalog() {
    try {
      return this.realConnection.getCatalog();
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  public Flux<ResultSet> getAllSchemas() {
    return wrapMany(() -> getSchemasSafe());
  }

  @Nonnull
  public Mono<ResultSet> getSchema(@Nonnull final String schema) {
    return wrapSingle(() -> getSchemaSafe(schema));
  }

  @Nonnull
  public Flux<ResultSet> getAllTables(
          @Nonnull final String schema, @Nonnull final String... tableTypes) {
    return wrapMany(() -> getTablesSafe(schema, tableTypes));
  }

  @Nonnull
  public Mono<ResultSet> getTable(
          @Nonnull final String schema, @Nonnull final String name, @Nonnull final String... tableTypes) {
    return wrapSingle(() -> getTableSafe(schema, name, tableTypes));
  }

  @Nonnull
  public Flux<ResultSet> getPrimaryKeys(@Nonnull final String schema, @Nonnull final String name) {
    return wrapMany(() -> getPrimaryKeysSafe(schema, name));
  }

  @Nonnull
  public Flux<ResultSet> getExportedKeys(@Nonnull final String schema, @Nonnull final String name) {
    return wrapMany(() -> getExportedKeysSafe(schema, name));
  }

  @Nonnull
  public Flux<ResultSet> getImportedKeys(@Nonnull final String schema, @Nonnull final String name) {
    return wrapMany(() -> getImportedKeysSafe(schema, name));
  }

  @Nonnull
  public Flux<ResultSet> getTablePrivileges(@Nonnull final String schema, @Nonnull final String name) {
    return wrapMany(() -> getTablePrivilegesSafe(schema, name));
  }

  @Nonnull
  public Flux<ResultSet> getAllColumns(
          @Nonnull final String schema, @Nonnull final String table) {
    return wrapMany(() -> getColumnsSafe(schema, table));
  }

  @Nonnull
  public Mono<ResultSet> getColumn(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {
    return wrapSingle(() -> getColumnSafe(schema, table, column));
  }

  @Nonnull
  public Flux<ResultSet> getColumnPrivileges(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {
    return wrapMany(() -> getColumnPrivilegesSafe(schema, table, column));
  }

  @Nonnull
  public Mono<Boolean> isPrimaryColumn(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {

    return query("SHOW KEYS FROM " + schema + "." + table + " WHERE Key_name = 'PRIMARY' and Column_name = '" + column + "'")
            .singleOrEmpty().hasElement();
  }

  @Nonnull
  public Flux<ResultSet> getReference(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {

    return query("SELECT REFERENCED_TABLE_SCHEMA AS TABLE_SCHEMA, REFERENCED_TABLE_NAME AS TABLE_NAME, REFERENCED_COLUMN_NAME AS COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = '" + schema + "' AND TABLE_NAME = '" + table + "' AND REFERENCED_COLUMN_NAME IS NOT NULL AND COLUMN_NAME = '" + column + "'");
  }

  @Nonnull
  public Flux<ResultSet> selectAll(String schema, String table, String start, String limit) {
    return query("SELECT * FROM " + schema + "." + table + " LIMIT " + start + ", " + limit);
  }

  @Nonnull
  public Flux<ResultSet> query(@Nonnull final String query) {
    return wrapMany(() -> querySafe(query));
  }

  @Nonnull
  private ResultSet getSchemasSafe() {
    try {
      return realConnection.getMetaData().getCatalogs();
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getSchemaSafe(@Nonnull final String schema) {
    try {
      return realConnection.createStatement().executeQuery(
              "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + schema + "'");
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getTableSafe(
          @Nonnull final String schema, @Nonnull final String tableName, @Nonnull final String... types) {
    try {
      return this.realConnection.getMetaData().getTables(schema, null, tableName, types);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getTablesSafe(@Nonnull final String schema, @Nonnull final String... types) {
    return getTableSafe(schema, "%", types);
  }

  @Nonnull
  private ResultSet getPrimaryKeysSafe(@Nonnull final String schema, @Nonnull final String table) {
    try {
      return realConnection.getMetaData().getPrimaryKeys(schema, null, table);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getExportedKeysSafe(@Nonnull final String schema, @Nonnull final String table) {
    try {
      return realConnection.getMetaData().getExportedKeys(schema, null, table);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getImportedKeysSafe(@Nonnull final String schema, @Nonnull final String table) {
    try {
      return realConnection.getMetaData().getImportedKeys(schema, null, table);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getTablePrivilegesSafe(@Nonnull final String schema, @Nonnull final String table) {
    try {
      return realConnection.getMetaData().getTablePrivileges(schema, null, table);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getColumnsSafe(@Nonnull final String schema, @Nonnull final String table) {
    return getColumnSafe(schema, table, "%");
  }

  @Nonnull
  private ResultSet getColumnPrivilegesSafe(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {
    try {
      return realConnection.getMetaData().getColumnPrivileges(schema, null, table, column);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet getColumnSafe(
          @Nonnull final String schema, @Nonnull final String table, @Nonnull final String column) {
    try {
      return realConnection.getMetaData().getColumns(schema, null, table, column);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private ResultSet querySafe(@Nonnull final String query) {
    try {
      return realConnection.createStatement().executeQuery(query);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  @Nonnull
  private Flux<ResultSet> wrapMany(@Nonnull final Supplier<ResultSet> resultSetSupplier) {
    this.openResults.incrementAndGet();
    return Flux.using(resultSetSupplier::get, result -> Flux.create(sink -> {
      try {
        while (result.next()) {
          sink.next(result);
        }
        sink.complete();
      } catch (SQLException e) {
        sink.error(e);
      }
    }), this::closeResultSet);
  }

  @Nonnull
  private Mono<ResultSet> wrapSingle(@Nonnull final Supplier<ResultSet> resultSetSupplier) {
    this.openResults.incrementAndGet();
    return Mono.using(resultSetSupplier::get, result -> Mono.create(sink -> {
      try {
        if (result.next()) {
          sink.success(result);
        }
        sink.success();
      } catch (SQLException e) {
        sink.error(e);
      }
    }), this::closeResultSet);
  }

  private void closeResultSet(@Nonnull final ResultSet resultSet) {
    try {
      // in case that this instance is marked as closed with last resultSet also close connection
      if (this.closed.get() && this.openResults.decrementAndGet() == 0) {
        resultSet.close();
        this.realConnection.close();
      } else {
        resultSet.close();
      }
    } catch (Exception e) {
      throw new DBConnectorException(e);
    }
  }

  public void close() {
    // mark this instance as closed
    this.closed.set(true);
    // in case there are no open resultSets close connection
    // otherwise connection will be closed with the last closed
    // resultSet
    if (this.realConnection != null && this.openResults.get() == 0) {
      try {
        this.realConnection.close();
      } catch (Exception e) {
        throw new DBConnectorException(e);
      }
    }
  }

  private static class DebugLogWriter extends Writer {

    final private Logger logger;

    public DebugLogWriter(final Logger logger) {
      this.logger = logger;
    }

    @Override
    public void write(@Nonnull char[] cbuf, int off, int len) {
      this.logger.debug(String.valueOf(cbuf, off, len));
    }

    @Override
    public void flush() {

    }

    @Override
    public void close() {

    }
  }
}
