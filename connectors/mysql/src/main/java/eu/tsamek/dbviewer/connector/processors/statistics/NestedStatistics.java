package eu.tsamek.dbviewer.connector.processors.statistics;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.processors.MySqlNestedProcessor;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public interface NestedStatistics extends MySqlNestedProcessor {

  @Nonnull
  Mono<DBDescription> statistics(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context);
}
