package eu.tsamek.dbviewer.connector.processors.previewers;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.processors.MySqlNestedProcessor;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public interface NestedPreviewer extends MySqlNestedProcessor {

  @Nonnull
  Mono<DBDescription> preview(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context);

}
