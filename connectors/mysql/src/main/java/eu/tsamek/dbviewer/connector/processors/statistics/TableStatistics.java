package eu.tsamek.dbviewer.connector.processors.statistics;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Consumer;

public class TableStatistics implements NestedStatistics {

  @Nonnull
  @Override
  public Mono<DBDescription> statistics(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    final var options = context.getOptions();

    final var schema = extractSchema(connector, path);
    final var leafObjectValue = path.get(path.getLeafObject());

    if (leafObjectValue == null || "*".equals(leafObjectValue)) {
      throw new DBFormatException("Name of the object has to be specified.");
    }

    return connector.getTable(schema, leafObjectValue, null).flatMap(resultSet -> {

      final Map<String, Object> stats = Maps.newLinkedHashMap();
      connector.query("select count(*) as NUM_RECORDS from " + schema + "." + leafObjectValue)
               .doOnNext(createConsumer(stats, "NUM_RECORDS")).blockFirst();

      connector.query("select count(*) as NUM_ATTRIBUTES FROM information_schema.columns WHERE table_schema='" + schema + "' AND table_name = '" + leafObjectValue + "'")
               .doOnNext(createConsumer(stats, "NUM_ATTRIBUTES"))
               .blockFirst();

      return Mono.just(stats);
    }).map(properties -> getDbDescription(schema, leafObjectValue, properties));
  }

  @Nonnull
  private Consumer<ResultSet> createConsumer(@Nonnull final  Map<String, Object> stats, @Nonnull final String propName) {
    return resultSet -> stats.computeIfAbsent(propName, (key) -> {
      try {
        return resultSet.getObject(key);
      } catch (SQLException e) {
        return null;
      }
    });
  }

  @Nonnull
  private DBDescription getDbDescription(
          @Nonnull final String schema,
          @Nonnull final String leafObjectValue,
          @Nonnull final Map<String, Object> properties) {
    return new DBDescription().setName("statistic of " + formatObjectNames(schema, leafObjectValue)).setDescription(
            "Statistic of table " + formatObjectNames(schema, leafObjectValue) + ".").setParameters(properties);
  }
}
