package eu.tsamek.dbviewer.connector.processors.descriptors;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.MySqlService;
import eu.tsamek.dbviewer.connector.beans.Column;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Map;

public class ColumnDescriptor implements NestedDescriptor {

  @Nonnull
  @Override
  public Mono<DBDescription> describe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {
    final var path = context.getPath();
    final var schema = extractSchema(connector, path);
    final var leafObject = path.getLeafObject();
    final var leafObjectValue = path.get(leafObject);

    validateSchema(schema);
    final var table = path.getFirstNonnull(MySqlService.MySQLObjects.TABLE, MySqlService.MySQLObjects.VIEW);
    validateTable(table);

    if ("*".equals(leafObjectValue)) {
      final var columnNames = getColumns(connector, schema, table);
      return Mono.just(new DBDescription().setName("column in " + formatObjectNames(schema, table))
                                          .setDescription("List of columns in table " + formatObjectNames(schema,
                                                  table) + ".")
                                          .setItems(columnNames));
    } else {
      return getColumn(connector, schema, table, leafObjectValue).map(columnProps -> {

        final Map<String, Object> properties = Maps.newLinkedHashMap();
        final var privileges = connector.getColumnPrivileges(schema, table, leafObjectValue).map(this::convertAllToMap);
        properties.put("isPrimaryKey", connector.isPrimaryColumn(schema, table, leafObjectValue));
        properties.put("referencesTo", connector.getReference(schema, table, leafObjectValue).map(this::convertAllToMap).singleOrEmpty());
        properties.put("properties", columnProps);
        properties.put("privileges", privileges.cache());

        return getDbDescription(schema, leafObjectValue, table, properties);
      });
    }
  }

  private DBDescription getDbDescription(
          String schema, String leafObjectValue, String table, Map<String, Object> properties) {
    return new DBDescription().setName("column " + formatObjectNames(schema, table, leafObjectValue)).setParameters(
            properties).setDescription("Description of column " + formatObjectNames(schema,
            table,
            leafObjectValue) + ".").setItems(Flux.empty());
  }

  @Nonnull
  private Mono<Map<String, Object>> getColumn(
          @Nonnull final MySqlConnector connector,
          @Nonnull final String schema,
          @Nonnull final String table,
          @Nonnull final String column) {

    return connector.getColumn(schema, table, column).map(this::convertAllToMap);
  }

  @Nonnull
  private Flux<Column> getColumns(
          @Nonnull final MySqlConnector connector, @Nonnull final String schema, @Nonnull final String table) {

    return connector.getAllColumns(schema, table).map(resultSet -> {
      try {
        final var text = resultSet.getString("COLUMN_NAME");
        final var type = resultSet.getString("TYPE_NAME");
        final var realTable = resultSet.getString("TABLE_NAME");
        final var realSchema = resultSet.getString("TABLE_CAT");
        return new Column().setName(text).setSchema(realSchema).setTable(realTable).setType(type);
      } catch (SQLException e) {
        throw new DBConnectorException(e);
      }
    });
  }
}
