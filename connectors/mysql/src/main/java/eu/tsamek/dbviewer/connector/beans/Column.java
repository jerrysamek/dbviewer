package eu.tsamek.dbviewer.connector.beans;

public class Column {
  private String name;
  private String schema;
  private String table;
  private String type;

  public String getName() {
    return name;
  }

  public Column setName(String name) {
    this.name = name;
    return this;
  }

  public String getSchema() {
    return schema;
  }

  public Column setSchema(String schema) {
    this.schema = schema;
    return this;
  }

  public String getTable() {
    return table;
  }

  public Column setTable(String table) {
    this.table = table;
    return this;
  }

  public String getType() {
    return type;
  }

  public Column setType(String type) {
    this.type = type;
    return this;
  }
}
