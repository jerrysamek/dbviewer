package eu.tsamek.dbviewer.connector.processors;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBObjectPath;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.MySqlService;
import org.apache.commons.lang3.StringUtils;
import reactor.core.publisher.Flux;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;

public interface MySqlNestedProcessor {

  @Nullable
  default String extractSchema(@Nonnull final MySqlConnector connector, @Nonnull final DBObjectPath path) {
    var schema = path.getFirstNonnull(MySqlService.MySQLObjects.SCHEMA, MySqlService.MySQLObjects.DATABASE);
    if (Objects.isNull(schema)) {
      schema = connector.getCatalog();
    }
    return schema;
  }

  @Nonnull
  default Map<String, Object> convertAllToMap(@Nonnull final ResultSet resultSet) {
    try {
      final var metadata = resultSet.getMetaData();
      final var size = metadata.getColumnCount();
      final Map<String, Object> result = Maps.newLinkedHashMap();
      for (int i = 0; i < size; i++) {
        result.put(metadata.getColumnLabel(i + 1), resultSet.getObject(i + 1));
      }
      return result;
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }

  default void validateSchema(@Nullable final String schema) {
    if (StringUtils.isEmpty(schema)) {
      throw new DBConnectorException(
              "Schema can not be null! Connection has to point to one or you have to use it in path!");
    }
  }

  default void validateTable(@Nullable final String table) {
    if (StringUtils.isEmpty(table)) {
      throw new DBConnectorException("Table can not be null!");
    }
  }

  @Nonnull
  default String formatObjectNames(@Nullable final String schema, @Nullable final String... parts) {
    final StringBuilder builder = new StringBuilder();
    builder.append(Objects.requireNonNullElse(schema, "<all schemas>"));

    if (parts != null) {
      Flux.fromArray(parts).doOnNext(string -> builder.append("/").append(string)).blockLast();
    }

    return builder.toString();
  }
}
