package eu.tsamek.dbviewer.connector.processors.dataaccessors;

import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.processors.MySqlNestedProcessor;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public interface NestedDataAccessor extends MySqlNestedProcessor {

  boolean supports(@Nonnull final String query);

  @Nonnull
  Mono<DBDescription> executeQuery(
          @Nonnull final MySqlConnector connector, @Nonnull final String query);
}
