package eu.tsamek.dbviewer.connector.beans;

public class Schema {
  private String name;
  private boolean current;

  public String getName() {
    return name;
  }

  public Schema setName(String name) {
    this.name = name;
    return this;
  }

  public boolean isCurrent() {
    return current;
  }

  public Schema setCurrent(boolean current) {
    this.current = current;
    return this;
  }
}
