package eu.tsamek.dbviewer.connector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectionParameter;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.api.DBService;
import eu.tsamek.dbviewer.connector.processors.dataaccessors.NestedDataAccessor;
import eu.tsamek.dbviewer.connector.processors.dataaccessors.TableSelectAccessor;
import eu.tsamek.dbviewer.connector.processors.descriptors.ColumnDescriptor;
import eu.tsamek.dbviewer.connector.processors.descriptors.ConnectionDescriptor;
import eu.tsamek.dbviewer.connector.processors.descriptors.DatabaseDescriptor;
import eu.tsamek.dbviewer.connector.processors.descriptors.NestedDescriptor;
import eu.tsamek.dbviewer.connector.processors.descriptors.TableDescriptor;
import eu.tsamek.dbviewer.connector.processors.previewers.NestedPreviewer;
import eu.tsamek.dbviewer.connector.processors.previewers.TablePreviewer;
import eu.tsamek.dbviewer.connector.processors.statistics.ColumnStatistics;
import eu.tsamek.dbviewer.connector.processors.statistics.NestedStatistics;
import eu.tsamek.dbviewer.connector.processors.statistics.TableStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class MySqlService implements DBService {

  private static final Logger logger = LoggerFactory.getLogger(MySqlService.class);

  public static final Set<? extends DBObject> ROOT_OBJECTS = EnumSet.of(MySQLObjects.SCHEMA,
          MySQLObjects.DATABASE,
          MySQLObjects.TABLE,
          MySQLObjects.VIEW);

  public static final Map<MySQLObjects, NestedDescriptor> NESTED_DESCRIPTORS = Maps.newEnumMap(MySQLObjects.class);
  public static final Map<MySQLObjects, NestedPreviewer> NESTED_PREVIEWERS = Maps.newEnumMap(MySQLObjects.class);
  public static final Map<MySQLObjects, NestedStatistics> NESTED_STATISTIC_RUNNERS = Maps.newEnumMap(MySQLObjects.class);
  public static final Collection<NestedDataAccessor> NESTED_DATA_ACCESSORS = Lists.newArrayList(new TableSelectAccessor());

  static {
    NESTED_DESCRIPTORS.put(MySQLObjects.CONNECTION, new ConnectionDescriptor());
    NESTED_DESCRIPTORS.put(MySQLObjects.DATABASE, new DatabaseDescriptor());
    NESTED_DESCRIPTORS.put(MySQLObjects.SCHEMA, new DatabaseDescriptor());
    NESTED_DESCRIPTORS.put(MySQLObjects.TABLE, new TableDescriptor());
    NESTED_DESCRIPTORS.put(MySQLObjects.VIEW, new TableDescriptor());
    NESTED_DESCRIPTORS.put(MySQLObjects.COLUMN, new ColumnDescriptor());

    NESTED_PREVIEWERS.put(MySQLObjects.TABLE, new TablePreviewer());
    NESTED_PREVIEWERS.put(MySQLObjects.VIEW, new TablePreviewer());

    NESTED_STATISTIC_RUNNERS.put(MySQLObjects.TABLE, new TableStatistics());
    NESTED_STATISTIC_RUNNERS.put(MySQLObjects.VIEW, new TableStatistics());
    NESTED_STATISTIC_RUNNERS.put(MySQLObjects.COLUMN, new ColumnStatistics());
  }

  @Nonnull
  @Override
  public Collection<DBConnectionParameter> supportedParameters() {
    return Lists.newArrayList(new DBConnectionParameter().setName("host")
                                                         .setDataType(DBConnectionParameter.DataType.STRING)
                                                         .setRequired(true),
            new DBConnectionParameter().setName("port")
                                       .setDataType(DBConnectionParameter.DataType.INTEGER)
                                       .setRequired(true)
                                       .setDefaultValue("3306"),
            new DBConnectionParameter().setName("dbname")
                                       .setDataType(DBConnectionParameter.DataType.STRING)
                                       .setRequired(false)
                                       .setDefaultValue(""),
            new DBConnectionParameter().setName("username")
                                       .setDataType(DBConnectionParameter.DataType.STRING)
                                       .setRequired(true),
            new DBConnectionParameter().setName("password")
                                       .setDataType(DBConnectionParameter.DataType.STRING)
                                       .setRequired(true)
                                       .setSecret(true));
  }

  @Nonnull
  @Override
  public String databaseType() {
    return "MySQL";
  }

  @Nonnull
  @Override
  public String version() {
    return "0.1.0";
  }

  @Nonnull
  @Override
  public Collection<? extends DBObject> supportedDbObjects() {
    return EnumSet.complementOf(EnumSet.of(MySQLObjects.CONNECTION));
  }

  @Nonnull
  @Override
  public Collection<? extends DBObject> rootDbObjects() {
    return ROOT_OBJECTS;
  }

  @Nonnull
  @Override
  public Mono<DBDescription> describe(@Nonnull final DBConnectorContext context) {
    logger.info("Describing ...");

    return Mono.using(() -> createConnector(context.getParameters()),
            connector -> doDescribe(connector, context),
            MySqlConnector::close);
  }

  @Nonnull
  @Override
  public Mono<DBDescription> preview(@Nonnull final DBConnectorContext context) {
    logger.info("Running preview ...");

    return Mono.using(() -> createConnector(context.getParameters()),
            connector -> doPreview(connector, context),
            MySqlConnector::close);
  }

  @Nonnull
  @Override
  public Mono<DBDescription> statistic(@Nonnull final DBConnectorContext context) {
    logger.info("Running statistic ...");

    return Mono.using(() -> createConnector(context.getParameters()),
            connector -> doStatistic(connector, context),
            MySqlConnector::close);
  }

  @Nonnull
  @Override
  public Mono<DBDescription> query(@Nonnull final DBConnectorContext context) {
    logger.info("Running query ...");

    return Mono.using(() -> createConnector(context.getParameters()),
            connector -> doQuery(connector, context),
            MySqlConnector::close);
  }

  @Nonnull
  private Mono<DBDescription> doPreview(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    if (!path.isEmpty()) {
      final var previewer = NESTED_PREVIEWERS.get(path.getLeafObject());
      if (Objects.nonNull(previewer)) {
        return previewer.preview(connector, context);
      }
    }

    throw new DBFormatException("Can not generate preview for given object!");
  }

  @Nonnull
  private Mono<DBDescription> doDescribe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    final NestedDescriptor descriptor;

    if (path.isEmpty()) {
      descriptor = NESTED_DESCRIPTORS.get(MySQLObjects.CONNECTION);
    } else {
      descriptor = NESTED_DESCRIPTORS.get(path.getLeafObject());
    }

    if (descriptor != null) {
      return descriptor.describe(connector, context);
    }

    throw new DBFormatException("Can not generate description for given object!");
  }

  @Nonnull
  private Mono<DBDescription> doStatistic(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();

    if (!path.isEmpty()) {
      final var statistics = NESTED_STATISTIC_RUNNERS.get(path.getLeafObject());

      if (statistics != null) {
        return statistics.statistics(connector, context);
      }
    }

    throw new DBFormatException("Can not generate statistic for given object!");
  }

  @Nonnull
  private Mono<DBDescription> doQuery(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {
    final String query = context.getQuery();

    if (query.isEmpty()) {
      throw new DBFormatException("Query is empty!");
    }

    final var accessor = Flux.fromIterable(NESTED_DATA_ACCESSORS)
                             .filter(nestedDataAccessor -> nestedDataAccessor.supports(query))
                             .single()
                             .blockOptional();

    if (!accessor.isPresent()) {
      throw new DBFormatException("This query is unsupported. Only select queries are allowed!");
    }

    return accessor.get().executeQuery(connector, query);
  }

  @Nonnull
  private MySqlConnector createConnector(@Nonnull final Map<String, String> parameters) {
    try {
      final var host = parameters.get("host");
      final var port = parameters.get("port");
      final var dbName = parameters.get("dbname");
      final var username = parameters.get("username");
      final var password = parameters.get("password");
      // wrapper is used to provide only necessary api and to have control over access to resultSets and statements
      return new MySqlConnector(
              "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
              username,
              password);
    } catch (SQLException e) {
      logger.warn("Can not access database with given parameters.", e);
      throw new DBFormatException("Can not access database with given parameters.");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new DBConnectorException(e);
    }
  }

  public enum MySQLObjects implements DBObject {
    CONNECTION, SCHEMA, DATABASE, TABLE, VIEW, COLUMN;

    @Override
    public Collection<MySQLObjects> next() {
      switch (this) {
        case DATABASE:
        case SCHEMA:
          return EnumSet.of(TABLE, VIEW);
        case TABLE:
        case VIEW:
          return EnumSet.of(COLUMN);
        case COLUMN:
        default:
          return EnumSet.noneOf(MySQLObjects.class);
      }
    }
  }
}
