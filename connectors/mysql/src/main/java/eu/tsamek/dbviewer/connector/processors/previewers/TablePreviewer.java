package eu.tsamek.dbviewer.connector.processors.previewers;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public class TablePreviewer implements NestedPreviewer {

  @Nonnull
  @Override
  public Mono<DBDescription> preview(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    final var options = context.getOptions();

    final var schema = extractSchema(connector, path);
    final var leafObjectValue = path.get(path.getLeafObject());

    if (leafObjectValue == null || "*".equals(leafObjectValue)) {
      throw new DBFormatException("Name of the object has to be specified.");
    }

    final var start = options.getOrDefault("offset", "0");
    final var limit = options.getOrDefault("limit", "100");

    return connector.getTable(schema, leafObjectValue, null).map(resultSet -> getDbDescription(connector,
            schema,
            leafObjectValue,
            start,
            limit));
  }

  @Nonnull
  private DBDescription getDbDescription(
          @Nonnull final MySqlConnector connector,
          @Nonnull final String schema,
          @Nonnull final String leafObjectValue,
          @Nonnull final String start,
          @Nonnull final String limit) {
    return new DBDescription().setName("preview of " + formatObjectNames(schema, leafObjectValue)).setDescription(
            "Preview of table " + formatObjectNames(schema, leafObjectValue) + ".").setItems(connector.selectAll(schema,
            leafObjectValue,
            start,
            limit).map(this::convertAllToMap));
  }
}
