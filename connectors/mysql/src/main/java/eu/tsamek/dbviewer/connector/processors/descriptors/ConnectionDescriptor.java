package eu.tsamek.dbviewer.connector.processors.descriptors;

import com.google.common.collect.Maps;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.MySqlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Function;

public class ConnectionDescriptor implements NestedDescriptor {

  private static final Logger logger = LoggerFactory.getLogger(ConnectionDescriptor.class);

  @Nonnull
  @Override
  public Mono<DBDescription> describe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    String description;
    final Map<String, Object> params = Maps.newLinkedHashMap();
    try {
      final var metaData = connector.getMetaData();

      description = "Connected to " + metaData.getDatabaseProductName() + " " + metaData.getDatabaseProductVersion();

      params.put("readOnly", metaData.isReadOnly());
      params.put("allProceduresAreCallable", metaData.allProceduresAreCallable());
      params.put("allTablesAreSelectable", metaData.allTablesAreSelectable());

      final var properties = Flux.fromArray(metaData.getClass().getDeclaredMethods())
                                 .filter(this::isNoParamGetMethod)
                                 .collectMap(getNameExtractor("get"),
                                         getValueExtractor(metaData),
                                         Maps::newLinkedHashMap);

      params.put("properties", properties);


      final var supports = Flux.fromArray(metaData.getClass().getDeclaredMethods())
                               .filter(this::isSupportsMethod)
                               .collectMap(getNameExtractor("supports"),
                                       getValueExtractor(metaData),
                                       Maps::newLinkedHashMap);

      params.put("supports", supports);
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }

    return Mono.just(new DBDescription().setName("connection")
                                        .setDescription(description)
                                        .setParameters(params)
                                        .setItems(Flux.fromIterable(MySqlService.ROOT_OBJECTS)
                                                      .map(DBObject::name)
                                                      .map(String::toLowerCase)));
  }

  private boolean isNoParamGetMethod(@Nonnull final Method method) {
    return Modifier.isPublic(method.getModifiers()) && method.getName()
                                                             .startsWith("get") && method.getParameterCount() == 0 && (method
            .getReturnType()
            .equals(String.class) || method.getReturnType().equals(int.class) || method.getReturnType()
                                                                                       .equals(boolean.class) || method.getReturnType()
                                                                                                                       .equals(long.class) || method
            .getReturnType()
            .equals(Integer.class) || method.getReturnType().equals(Boolean.class) || method.getReturnType()
                                                                                            .equals(Long.class));
  }

  private boolean isSupportsMethod(@Nonnull final Method method) {
    return Modifier.isPublic(method.getModifiers()) && method.getName()
                                                             .startsWith("supports") && method.getParameterCount() == 0;
  }


  @Nonnull
  private Function<Method, String> getNameExtractor(@Nonnull final String prefix) {
    return method -> method.getName().substring(prefix.length());
  }

  @Nonnull
  private Function<Method, Object> getValueExtractor(@Nonnull final DatabaseMetaData metaData) {
    return method -> {
      try {
        return method.invoke(metaData);
      } catch (IllegalAccessException | InvocationTargetException e) {
        logger.debug("Can not access method.", e);
      }
      return null;
    };
  }

}
