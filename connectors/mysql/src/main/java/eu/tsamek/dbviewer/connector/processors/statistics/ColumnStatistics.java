package eu.tsamek.dbviewer.connector.processors.statistics;

import com.google.common.collect.Sets;
import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.MySqlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

public class ColumnStatistics implements NestedStatistics {

  private static final Logger logger = LoggerFactory.getLogger(ColumnStatistics.class);

  private static final Set<String> NO_AVG_MEDIAN_TYPES = Sets.newHashSet("CHAR",
          "VARCHAR",
          "BINARY",
          "VARBINARY",
          "BLOB",
          "TEXT",
          "ENUM",
          "SET");

  @Nonnull
  @Override
  public Mono<DBDescription> statistics(
          @Nonnull MySqlConnector connector, @Nonnull DBConnectorContext context) {
    final var path = context.getPath();
    final var options = context.getOptions();

    final var schema = extractSchema(connector, path);
    validateSchema(schema);
    final var table = path.getFirstNonnull(MySqlService.MySQLObjects.TABLE, MySqlService.MySQLObjects.VIEW);
    validateTable(table);
    final var column = path.get(path.getLeafObject());

    if (column == null || "*".equals(column)) {
      throw new DBFormatException("Name of the column has to be specified.");
    }

    return connector.getColumn(schema, table, column).map(resultSet -> {
      try {
        final var type = resultSet.getString("TYPE_NAME");
        return !NO_AVG_MEDIAN_TYPES.contains(type.toUpperCase());
      } catch (Throwable e) {
        logger.debug(e.getMessage(), e);
      }
      return false;
    }).flatMapMany(supports -> queryColumnStatistics(connector, schema, table, column, supports)).singleOrEmpty();
  }

  @Nonnull
  private Flux<DBDescription> queryColumnStatistics(
          @Nonnull final MySqlConnector connector,
          @Nonnull final String schema,
          @Nonnull final String table,
          @Nonnull final String column,
          @Nonnull final Boolean supports) {

    return connector.query("select min(" + column + ") as MIN_VALUE, max(" + column + ") as MAX_VALUE, avg(" + column + ") as AVERAGE_VALUE, count(" + column + ") as NUM_RECORDS from " + schema + "." + table)
                    .map(this::convertAllToMap)
                    .map(properties -> {
                      if (supports) {
                        final Long records = (Long) properties.get("NUM_RECORDS");
                        long start, limit;
                        if (records % 2 == 0) {
                          start = records / 2 - 1;
                          limit = 2;
                        } else {
                          start = records / 2;
                          limit = 1;
                        }
                        var median = connector.query("select avg(t1." + column + ") as MEDIAN from (select " + column + " from " + schema + "." + table + " order by " + column + " limit " + start + ", " + limit + ") as t1")
                                              .map(rs -> {
                                                try {
                                                  return rs.getObject("MEDIAN");
                                                } catch (SQLException e) {
                                                  throw new RuntimeException(e);
                                                }
                                              })
                                              .singleOrEmpty();
                        properties.put("MEDIAN", median);
                      } else {
                        properties.remove("AVERAGE_VALUE");
                      }
                      return getDbDescription(schema, table, column, properties);
                    });
  }

  @Nonnull
  private DBDescription getDbDescription(
          @Nonnull final String schema,
          @Nonnull final String table,
          @Nonnull final String column,
          @Nonnull final Map<String, Object> properties) {
    return new DBDescription().setName("statistic of " + formatObjectNames(schema, table, column)).setDescription(
            "Statistic of column " + formatObjectNames(schema, table, column) + ".").setParameters(properties);
  }
}
