package eu.tsamek.dbviewer.connector.processors.descriptors;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.processors.MySqlNestedProcessor;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public interface NestedDescriptor extends MySqlNestedProcessor {

  @Nonnull
  Mono<DBDescription> describe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context);
}
