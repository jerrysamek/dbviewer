package eu.tsamek.dbviewer.connector.processors.descriptors;

import eu.tsamek.dbviewer.api.DBConnectorContext;
import eu.tsamek.dbviewer.api.DBConnectorException;
import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBObject;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import eu.tsamek.dbviewer.connector.beans.Schema;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseDescriptor implements NestedDescriptor {

  @Nonnull
  @Override
  public Mono<DBDescription> describe(
          @Nonnull final MySqlConnector connector, @Nonnull final DBConnectorContext context) {

    final var path = context.getPath();
    final var leafObject = path.getLeafObject();
    final var leafObjectValue = path.get(leafObject);

    if ("*".equals(leafObjectValue)) {
      final var schemaNames = getSchemas(connector);
      return Mono.just(new DBDescription().setName("schemas")
                                          .setDescription("List of schemas.")
                                          .setItems(schemaNames));
    } else {
      return connector.getSchema(leafObjectValue).map(resultSet -> getDbDescription(leafObject, leafObjectValue));
    }
  }

  @Nonnull
  private DBDescription getDbDescription(@Nonnull final DBObject leafObject, @Nonnull final String leafObjectValue) {
    return new DBDescription().setName("schema " + formatObjectNames(leafObjectValue))
                              .setDescription("Description of schema " + formatObjectNames(leafObjectValue) + ".")
                              .setItems(Flux.fromIterable(leafObject.next())
                                            .map(DBObject::name)
                                            .map(String::toLowerCase));
  }

  @Nonnull
  private Flux<Schema> getSchemas(@Nonnull final MySqlConnector connector) {
    return connector.getAllSchemas().map(resultSet -> convertToSchema(connector, resultSet));
  }

  private Schema convertToSchema(@Nonnull MySqlConnector connector, ResultSet resultSet) {
    try {
      final var text = resultSet.getString("TABLE_CAT");
      return new Schema().setName(text).setCurrent(text.equals(connector.getCatalog()));
    } catch (SQLException e) {
      throw new DBConnectorException(e);
    }
  }
}
