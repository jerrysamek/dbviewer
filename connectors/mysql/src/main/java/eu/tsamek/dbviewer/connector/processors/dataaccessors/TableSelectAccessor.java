package eu.tsamek.dbviewer.connector.processors.dataaccessors;

import eu.tsamek.dbviewer.api.DBDescription;
import eu.tsamek.dbviewer.api.DBFormatException;
import eu.tsamek.dbviewer.connector.MySqlConnector;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

public class TableSelectAccessor implements NestedDataAccessor {

  @Override
  public boolean supports(@Nonnull final String query) {
    // Quite a poor mans filtering :)
    return !query.toLowerCase().contains("delete") && !query.toLowerCase().contains("insert") && !query.toLowerCase()
                                                                                                       .contains(
                                                                                                               "update");
  }

  @Nonnull
  @Override
  public Mono<DBDescription> executeQuery(
          @Nonnull final MySqlConnector connector, @Nonnull final String query) {

    if (query.isEmpty()) {
      throw new DBFormatException("Query is empty!");
    }

    if (query.toLowerCase().contains("delete") || query.toLowerCase().contains("insert") || query.toLowerCase()
                                                                                                 .contains("update")) {
      throw new DBFormatException("Service does not support data modification!");
    }

    return Mono.just(new DBDescription().setName("Results")
                                        .setDescription("Results for query: " + query)
                                        .setItems(connector.query(query).map(this::convertAllToMap)));
  }
}
