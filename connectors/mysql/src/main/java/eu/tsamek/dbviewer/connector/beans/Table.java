package eu.tsamek.dbviewer.connector.beans;

public class Table {
  private String name;
  private String schema;
  private String type;

  public String getName() {
    return name;
  }

  public Table setName(String name) {
    this.name = name;
    return this;
  }

  public String getSchema() {
    return schema;
  }

  public Table setSchema(String schema) {
    this.schema = schema;
    return this;
  }

  public String getType() {
    return type;
  }

  public Table setType(String type) {
    this.type = type;
    return this;
  }
}
