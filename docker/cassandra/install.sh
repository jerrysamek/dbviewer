#!/usr/bin/env bash

type docker version > /dev/null 2>&1 || { echo >&2 "I require docker but it's not installed.  Aborting."; exit 1; }

cp ././../../core/src/main/resources/cassandra-init.cql cassandra-init.cql

docker run -d --name dbviewer_store --hostname dbviewer_store -p 7000:7000 -p 7001:7001 -p 7199:7199 -p 9042:9042 -p 9160:9160 -p 9142:9142 -d cassandra:latest

docker cp cassandra-init.cql dbviewer_store:/var/cassandra-init.cql

until docker exec dbviewer_store cqlsh -f /var/cassandra-init.cql; do
    echo "cqlsh: Cassandra is unavailable - retry later"
    sleep 2
done

echo "cqlsh: Cassandra keyspace initialized"