package eu.tsamek.dbviewer.api;

import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface DBService {

  @Nonnull
  Collection<DBConnectionParameter> supportedParameters();

  @Nonnull
  String databaseType();

  @Nonnull
  String version();

  @Nonnull
  Collection<? extends DBObject> supportedDbObjects();

  @Nonnull
  Collection<? extends DBObject> rootDbObjects();

  @Nonnull
  Mono<DBDescription> describe(@Nonnull final DBConnectorContext context);

  @Nonnull
  Mono<DBDescription> preview(@Nonnull final DBConnectorContext context);

  @Nonnull
  Mono<DBDescription> statistic(@Nonnull final DBConnectorContext context);

  @Nonnull
  Mono<DBDescription> query(@Nonnull final DBConnectorContext context);

  default DBService init() {
    return this;
  }

  default DBService destroy() {
    return this;
  }
}
