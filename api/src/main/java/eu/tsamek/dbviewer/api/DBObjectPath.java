package eu.tsamek.dbviewer.api;

import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class DBObjectPath {

  private static final DBObject EMPTY_OBJECT = new EmptyObject();

  private final Map<Integer, String> pathParts = Maps.newHashMap();
  private DBObject leaf = EMPTY_OBJECT;

  public void put(@Nonnull final DBObject dbObject, @Nullable final String value) {
    pathParts.put(dbObject.ordinal(), value);
    leaf = dbObject;
  }

  @Nullable
  public String get(@Nonnull final DBObject dbObject) {
    return pathParts.get(dbObject.ordinal());
  }

  @Nullable
  public String getFirstNonnull(@Nonnull final DBObject... dbObjects) {
    for (final DBObject dbObject : dbObjects) {
      final var val = pathParts.get(dbObject.ordinal());
      if (val != null) {
        return val;
      }
    }
    return null;
  }

  @Nullable
  public String getOrDefault(@Nonnull final DBObject dbObject, @Nullable final String defaultValue) {
    return pathParts.getOrDefault(dbObject.ordinal(), defaultValue);
  }

  @Nonnull
  public DBObject getLeafObject() {
    return leaf;
  }

  public boolean isEmpty() {
    return EMPTY_OBJECT.equals(leaf);
  }

  private static class EmptyObject implements DBObject {
    @Override
    public String name() {
      return ".empty";
    }

    @Override
    public int ordinal() {
      return 0;
    }

    @Override
    public Collection<? extends DBObject> next() {
      return Collections.emptyList();
    }
  }
}
