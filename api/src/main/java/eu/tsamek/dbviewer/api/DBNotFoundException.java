package eu.tsamek.dbviewer.api;

public class DBNotFoundException extends DBConnectorException {
  public DBNotFoundException(String message) {
    super(message);
  }
}
