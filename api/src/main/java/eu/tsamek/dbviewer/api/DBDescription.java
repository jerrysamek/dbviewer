package eu.tsamek.dbviewer.api;

import reactor.core.publisher.Flux;

import java.util.Map;

public class DBDescription<T> {

  private String name;
  private String description;
  private Flux<T> items;
  private Map<String, Object> parameters;

  public String getName() {
    return name;
  }

  public DBDescription setName(String name) {
    this.name = name;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public DBDescription setDescription(final String description) {
    this.description = description;
    return this;
  }

  public Flux<T> getItems() {
    return items;
  }

  public DBDescription setItems(final Flux<T> items) {
    this.items = items.cache();
    return this;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public DBDescription setParameters(final Map<String, Object> parameters) {
    this.parameters = parameters;
    return this;
  }
}
