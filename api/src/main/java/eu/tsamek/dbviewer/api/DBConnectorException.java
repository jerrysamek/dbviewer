package eu.tsamek.dbviewer.api;

public class DBConnectorException extends RuntimeException {

  public DBConnectorException(String message) {
    super(message);
  }

  public DBConnectorException(String message, Throwable cause) {
    super(message, cause);
  }

  public DBConnectorException(Throwable cause) {
    super(cause);
  }
}
