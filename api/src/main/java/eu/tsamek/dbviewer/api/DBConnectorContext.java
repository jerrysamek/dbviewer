package eu.tsamek.dbviewer.api;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;

public class DBConnectorContext {

  private Map<String, String> parameters = Collections.emptyMap();
  private Map<String, String> options = Collections.emptyMap();
  private DBObjectPath path = null;
  private String query = null;

  private DBConnectorContext() {
  }

  public Map<String, String> getParameters() {
    return parameters;
  }

  public Map<String, String> getOptions() {
    return options;
  }

  public DBObjectPath getPath() {
    return path;
  }

  public String getQuery() {
    return query;
  }

  public static Builder constructForPath(@Nonnull final DBObjectPath path) {
    final var instance = new DBConnectorContext();
    instance.path = path;
    return new Builder(instance);
  }

  public static Builder constructForQuery(@Nonnull final String query) {
    final var instance = new DBConnectorContext();
    instance.query = query;
    return new Builder(instance);
  }

  public static class Builder {
    private final DBConnectorContext instance;

    private Builder(@Nonnull final DBConnectorContext instance) {
      this.instance = instance;
    }

    public Builder withParameters(final Map<String, String> parameters) {
      instance.parameters = parameters;
      return this;
    }

    public Builder withOptions(final Map<String, String> options) {
      instance.options = options;
      return this;
    }

    public DBConnectorContext build() {
      return instance;
    }
  }
}
