package eu.tsamek.dbviewer.api;

public class DBConnectionParameter {

  private String name;
  private DataType dataType;
  private String description;
  private boolean required = true;
  private boolean secret = false;
  private String defaultValue;

  public String getName() {
    return name;
  }

  public DBConnectionParameter setName(String name) {
    this.name = name;
    return this;
  }

  public DataType getDataType() {
    return dataType;
  }

  public DBConnectionParameter setDataType(DataType dataType) {
    this.dataType = dataType;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public DBConnectionParameter setDescription(String description) {
    this.description = description;
    return this;
  }

  public boolean isRequired() {
    return required;
  }

  public DBConnectionParameter setRequired(boolean required) {
    this.required = required;
    return this;
  }

  public boolean isSecret() {
    return secret;
  }

  public DBConnectionParameter setSecret(boolean secret) {
    this.secret = secret;
    return this;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public DBConnectionParameter setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }

  public enum DataType {STRING, INTEGER}
}
