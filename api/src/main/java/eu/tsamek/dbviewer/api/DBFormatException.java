package eu.tsamek.dbviewer.api;

public class DBFormatException extends DBConnectorException {
  public DBFormatException(String message) {
    super(message);
  }
}
