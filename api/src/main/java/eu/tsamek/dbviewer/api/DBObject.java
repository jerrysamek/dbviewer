package eu.tsamek.dbviewer.api;

import java.util.Collection;

public interface DBObject {
  String name();

  int ordinal();

  Collection<? extends DBObject> next();
}
